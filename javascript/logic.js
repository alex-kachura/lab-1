/**
 * Created with JetBrains WebStorm.
 * User: Alex
 * Date: 22.03.13
 * Time: 16:53
 * To change this template use File | Settings | File Templates.
 */

function get_friends() {
    FB.api('/me/friends', function (response) {
        if (response.data) {
            var friends = [];
            $.each(response.data, function (key, friend) {
                friends.push('<li><input type="checkbox" id="' + friend.name + '" value="' + friend.id + '">' + friend.name + '</li>');
            });
            $('.friends').empty().append(friends.join(" "));
        }
    });
    $('.fr_next').show();
    $('.fr_share').hide();
}


function get_photos() {
    FB.api({
            method: 'fql.multiquery',
            queries:{
                query1: 'SELECT aid, type, name FROM album WHERE owner = me()',
                query2: 'SELECT pid, aid, src_small, src_small_width, src_small_height, src_big, src_big_width, src_big_height FROM photo WHERE aid IN ( SELECT aid FROM #query1 )'
            }
        },
        function(response){
            var photos = [];
            $.each(response[1].fql_result_set, function(key, friend) {
                photos.push('<label><input type="checkbox"><img src="' + friend.src_big + '" id="' + friend.pid + '"/></label>');
            });
            $('.photos').empty().append(photos.join(" "));
        }
    );
    $('.ph_next').show();
    $('.ph_send').hide();
}

$(document).ready(function() {
    $('.b_friends').toggle(function() {
        get_friends();
        $('.fr').show();
    }, function() { $('.fr').hide(); });

    $('.b_photos').toggle(function() {
        get_photos();
        $('.ph').show();
    }, function() { $('.ph').hide(); });

    $('.fr_next').on('click', function() {
        $('.friends input[type="checkbox"]').each(function(){
            if (!(this.checked)) {
                $(this).parents('li:first').remove();
            }
        });
        $('.fr_share').show();
        $('.fr_next').hide();
    });

    $('.fr_back').on('click', function() {
        $('.fr_share').hide();
        $(this).parents('span.clearfix:first').hide();
    });


    $('.fr_share').on('click', function(){
        var username = [];
        var userid = [];
        var fr_to_share = $('.friends input[type="checkbox"]:checked');
        fr_to_share.each(function(i){
            username[i] = $(this).attr('id');
            userid[i] = $(this).val();
            var obj = {
                method: 'feed',
                link: 'https://stark-coast-6455.herokuapp.com/',
                picture: 'http://s1.ipicture.ru/uploads/20130328/YFfm1RkU.jpg',
                name: 'Check it out',
                caption: 'Hello, ' + username[i] + '!',
                description: 'Facebook feed dialog. So much fun!',
                to: userid[i]
            };
            FB.ui(obj);
        })
    });

    $('.ph_next').on('click', function() {
        $('.photos input[type="checkbox"]').each(function(){
            if (!(this.checked)) {
                $(this).parent().remove();
            }
        });
        $('.ph_send').show();
        $('.ph_next').hide();
    });

    $('.ph_back').on('click', function() {
        $('.ph_send').hide();
        $(this).parents('span.clearfix:first').hide();
    });

    $('.ph_send').on('click', function(){
        var pid = [];
        $('.photos').children('img').each(function(i){
            pid[i] = $(this).attr('id');
        });
        $.ajax({
            data: {id:pid}
        });
    });
});